# Hash Manager

Store application state in the URL hash. Hash Manager is designed for use
in [Stencil][1] components.

## Usage
```typescript
import { Component, Listen, Prop, Watch } from '@stencil/core';
import { hashManager } from '@ccrpc/hash-manager';

@Component({
  tag: 'person',
})
export class Person {
  @Prop() name: string = hashManager.init('name', 'Jane');

  @Listen('hashManagerChange', {target: 'window'})
  updateName(e: CustomEvent) {
    if (e.detail.old.name !== e.detail.new.name) this.name = e.detail.new.name;
  }
  
  @Watch('name')
  nameChanged() {
    hashManager.set('name', this.name);
  }
}
```

## Credits
Hash Manager was developed by Matt Yoder for the [Champaign County Regional
Planning Commission][2].

## License
Hash Manager is available under the terms of the [BSD 3-clause license][3].

[1]: https://stenciljs.com/
[2]: https://ccrpc.org/
[3]: https://gitlab.com/ccrpc/hash-manager/blob/master/LICENSE.md
