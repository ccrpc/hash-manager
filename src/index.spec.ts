import { hashManager } from './index';

describe('hash manager', () => {
  beforeEach(() => {
    window.location.hash = '#map=12/40.11032/-88.22888&first=two';
  });

  test('hash is parsed correctly', () => {
    hashManager.init('first', 'one');
    expect(hashManager.get('first')).toBe('two');
  });

  test('init sets default value', () => {
    hashManager.init('foo', 'bar');
    expect(hashManager.get('foo')).toBe('bar');
  });

  test('default is not included in hash', () => {
    hashManager.init('foo', 'bar');
    hashManager.set('foo', 'bar');
    expect(window.location.hash.indexOf('foo')).toBe(-1);
  });

  test('non-default value is included in hash', () => {
    hashManager.init('foo', 'bar');
    hashManager.set('foo', 'baz');
    expect(window.location.hash.indexOf('foo=baz') > -1).toBe(true);
  });

  test('unmanaged keys are unchanged', () => {
    hashManager.init('foo', 'bar');
    hashManager.set('foo', 'baz');
    expect(window.location.hash.indexOf('first=two') > -1).toBe(true);
  });

  test('unmanaged keys are not returned by get', () => {
    expect(hashManager.get('map')).toBe(undefined);
  });
});
