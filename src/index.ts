class HashManager {
    defaults : any = {};
    hash: any = {};
  
    constructor() {
      window.addEventListener('hashchange', () => this.hashChanged(), false);
    }
  
    private getHashObject(all : boolean = false) {
      const hash = window.location.hash.replace('#', '');
      const obj = {...this.defaults};
      hash.split('&').map((part) => part.split('='))
        .filter((kv) => all || kv[0] in this.defaults)
        .forEach((kv) => {
          obj[kv[0]] = decodeURIComponent(kv[1]);
        });
      return obj;
    }

    private getUnmanagedString() {
      const hash = window.location.hash.replace('#', '');
      return hash.split('&').map((part) => part.split('='))
        .filter((kv) => !(kv[0] in this.defaults))
        .map((kv) => kv.join('='))
        .join('&');
    }
  
    private getHashString() {
      let kv = Object.keys(this.hash)
        .filter((k) => !(k in this.hash && this.hash[k] == this.defaults[k]))
        .map((k) => `${k}=${encodeURIComponent(this.hash[k])}`).join('&');
      let unmanaged = this.getUnmanagedString();
      kv = (kv) ? `${kv}&${unmanaged}` : unmanaged;
      return (kv) ? `#${kv}` : '';
    }
  
    private hashChanged() {
      let oldHash = {...this.hash};
      let newHash = this.getHashObject();
      let changed = Object.keys(this.defaults)
        .filter((k) => oldHash[k] !== newHash[k]);
      if (changed.length) this.fireChangeEvent(oldHash, newHash);
    }
  
    private fireChangeEvent(oldHash: any, newHash:any) {
      let ev = new CustomEvent('hashManagerChange', {
        detail: {
          old: oldHash,
          new: newHash
        }
      });
      window.dispatchEvent(ev);
    }
  
    public init(key: string, defaultValue: string) {
      let initial = this.getHashObject(true)[key];
      this.defaults[key] = defaultValue;
      this.hash[key] = (initial === undefined) ? defaultValue : initial;
      return this.hash[key];
    }
  
    public set(key: string, value: string) {
      if (this.hash[key] === value) return;
      this.hash[key] = value;
      let hashStr = this.getHashString();
      try {
        window.history.replaceState(window.history.state, '', hashStr);
      } catch (SecurityError) {}
    }
  
    public get(key: string) {
      return this.hash[key];
    }
  }
  
  export const hashManager = new HashManager();
  