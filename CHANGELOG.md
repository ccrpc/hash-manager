# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.2](https://gitlab.com/ccrpc/hash-manager/compare/v0.1.1...v0.1.2) (2020-02-04)

### [0.1.1](https://gitlab.com/ccrpc/hash-manager/compare/v0.1.0...v0.1.1) (2020-02-04)


### Bug Fixes

* remove debug logging ([c06bf1b](https://gitlab.com/ccrpc/hash-manager/commit/c06bf1bac6b1514ba39286c7d3ab3c2c41abab02)), closes [#1](https://gitlab.com/ccrpc/hash-manager/issues/1)

## 0.1.0 (2020-02-04)
